<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
use App\Http\Controllers\users\LoginController;
use App\Http\Controllers\products\ProductController;
use App\Http\Controllers\carts\CartController;

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    return "Cache is cleared";
});
Route::get('/', function () {
    return view('welcome');
});
//**admin/dashboard **
Route::get('admin/dashboard', function () {
    return view('dashboard');
});
//**users **
Route::group(['namespace' => 'users'], function() {
    Route::get('login', [LoginController::class, 'index']);
    Route::post('login', [LoginController::class, 'login']);
});
//**products **
Route::group(['namespace' => 'products'], function() {
    Route::get('products', [ProductController::class, 'index'])->name('products.list');
    Route::get('products/fetch_data', [ProductController::class, 'fetch_data']);
    Route::get('products/detail/{id}', [ProductController::class, 'detail'])->name('product.detail');
});
//**carts **
Route::group(['namespace' => 'carts'], function() {
//    Route::get('carts', [CartController::class, 'index'])->name('carts');
    Route::post('carts/data', [CartController::class, 'insert_cart']);
    Route::get('cart/detail', [CartController::class, 'detail'])->name('cart.detail');
    Route::post('cart/update', [CartController::class, 'update'])->name('cart.update');
     Route::post('cart/delete', [CartController::class, 'delete'])->name('cart.delete');
});
