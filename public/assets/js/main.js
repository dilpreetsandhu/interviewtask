// Initiate the wowjs animation library
  new WOW().init();
 $(document).ready(function() {
        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
          disableOn: 700,
          type: 'iframe',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,

          fixedContentPos: false
        });
      });
(function ($) {
  "use strict";

   $('#owl-testimonials').owlCarousel({
                margin: 20,
                loop: true,
                dots: false,
                autoPlay: 7000,
                items: 9,
				nav:false,
				itemsDesktop : [1199, 7],
        itemsDesktopSmall : [979, 5],
        itemsTablet : [768, 5],
        itemsTabletSmall : false,
        itemsMobile : [479, 3],
                 responsiveClass:true,
                 responsive:{
                     0:{
                         items:1,
                         nav:false
                     },
                     600:{
                       margin: 20,
                         items:2,
                         nav:false
                     },
                     1000:{
                         items:1,
                         nav:false,
                         loop:false
                     }
                }
            });
 $('#Related').owlCarousel({
                margin: 20,
                loop: true,
                dots: true,
                autoPlay: 3000,
                items: 3,
				nav:false,
				itemsDesktop : [1199, 2],
        itemsDesktopSmall : [979, 2],
        itemsTablet : [768, 1],
        itemsTabletSmall : false,
        itemsMobile : [479, 1],
                 responsiveClass:true,
                 responsive:{
                     0:{
                         items:1,
                         nav:false
                     },
                     600:{
                         items:1,
                         nav:false
                     },
                     1000:{
                         items:2,
                         nav:false,
                         loop:false
                     }
                }
            });

			  $('#owl-1').owlCarousel({
                margin: 20,
                loop: true,
                autoPlay: 3000,
                items: 1,
				nav:true,
				dots:true,
				navigation : true,
        navigationText : ["<img src='img/angle-1.png'>", "<img src='img/angle-2.png'>"],
				itemsDesktop : [1199, 1],
        itemsDesktopSmall : [979, 1],
        itemsTablet : [768, 1],
        itemsTabletSmall : false,
        itemsMobile : [479, 1],
                  responsiveClass:true,
                  responsive:{
                     0:{
                          items:1,
                         nav:false,
						  navigation : false,
						
                      },
                      600:{
                          items:1,
                         nav:false,
						 navigation : false,
                      },
                     1000:{
                         items:1,
                          nav:true,
                         loop:false,
						 navigation : true,
                     }
                 }
            });
  
 $('#owl-2').owlCarousel({
                margin: 0,
                loop: true,
                autoPlay: 3000,
                items: 1,
				itemsDesktop : [1199, 1],
				nav:true,
				dots:true,
                // responsiveClass:true,
                // responsive:{
                //     0:{
                //         items:1,
                //         nav:false
                //     },
                //     600:{
                //         items:1,
                //         nav:false
                //     },
                //     1000:{
                //         items:2,
                //         nav:true,
                //         loop:false
                //     }
                // }
            });
		


 

  // Initiate the wowjs animation library
  new WOW().init();

  // Header scroll class
  // $(window).scroll(function() {
  //   if ($(this).scrollTop() > 100) {
  //     $('#header').addClass('header-scrolled');
  //   } else {
  //     $('#header').removeClass('header-scrolled');
  //   }
  // });

  // if ($(window).scrollTop() > 100) {
  //   $('#header').addClass('header-scrolled');
  // }

  // Smooth scroll for the navigation and links with .scrollto classes
  $('.main-nav a, .mobile-nav a, .scrollto').on('click', function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      if (target.length) {
        var top_space = 0;

        if ($('#header').length) {
          top_space = $('#header').outerHeight();

          if (! $('#header').hasClass('header-scrolled')) {
            top_space = top_space - 40;
          }
        }

        $('html, header').animate({
          scrollTop: target.offset().top - top_space
        }, 1500, 'easeInOutExpo');

        if ($(this).parents('.main-nav, .mobile-nav').length) {
          $('.main-nav .active, .mobile-nav .active').removeClass('active');
          $(this).closest('li').addClass('active');
        }

        if ($('header').hasClass('mobile-nav-active')) {
          $('header').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('.mobile-nav-overly').fadeOut();
        }
        return false;
      }
    }
  });

  // Navigation active state on scroll
  var nav_sections = $('section');
  var main_nav = $('.main-nav, .mobile-nav');
  var main_nav_height = $('#header').outerHeight();

  $(window).on('scroll', function () {
    var cur_pos = $(this).scrollTop();
  
    nav_sections.each(function() {
      var top = $(this).offset().top - main_nav_height,
          bottom = top + $(this).outerHeight();
  
      if (cur_pos >= top && cur_pos <= bottom) {
        main_nav.find('li').removeClass('active');
        main_nav.find('a[href="#'+$(this).attr('id')+'"]').parent('li').addClass('active');
      }
    });
  });

  // jQuery counterUp (used in Whu Us section)
  $('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000
  });

 

 

 



       
				
				})(jQuery);
