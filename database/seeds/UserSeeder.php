<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(5) . '@gmail.com',
            'password' => Hash::make(123456),
            'role_id' => 2,
            'profile' => 'profile.jpg',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);
    }

}
