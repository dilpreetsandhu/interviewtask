<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProdectsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $id = DB::table('products')->insertGetId([
            'title' => Str::random(6),
            'description' => Str::random(100),
            'cover' => 'cover.jpg',
            'discount' => NULL,
            'price' => rand(50, 100),
            'added_by' => 1,
            'role' => 1,
            'status' => '1',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);
          
        $color = ['grey', 'red', 'blue', 'yellow'];
        $corR = array_rand($color);
        $size = ['S', 'M', 'L', 'XL', 'XXL'];
        $sizR = array_rand($size);
        DB::table('attributes')->insert([
            'product_id' => $id,
            'color' => $color[$corR],
            'size' => $size[$sizR],
            'qty' => rand(1, 10),
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);

        $len = rand(3, 4);
        for ($img = 1; $img <= $len; $img++) {
            $image = ['shop1.jpg', 'shop2.png', 'shop3.png', 'shop4.png'];
            $imgR = array_rand($image);
            DB::table('product_images')->insert([
                'product_id' => $id,
                'image' => $image[$imgR],
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ]);
        }
    }

}
