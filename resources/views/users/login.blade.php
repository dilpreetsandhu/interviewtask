@include('layout/header')


    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth px-0">
                <div class="row w-100 mx-0">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                            @if(Session::has('error'))
                            <div  class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif 
                            @if(Session::has('success'))
                            <div   class="alert {{ Session::get('alert-class', 'alert-success') }}">{!! session('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif


                            <h6 style="text-align: center;"><b>Hello! let's get started</b></h6>
                            <p class="font-weight-light" style='margin:20px;'><b>Sign in to continue.</b></p>
                            <form method="POST" id="login_action" action="{{ url('login') }}" class="local-form content-box">
                                @csrf
                                <div class="form-group">
                                    <label for="example-text">Email<sup style="color:red">*</sup></label>
                                    <input type="email" class="form-control form-control-lg  @error('email') is-invalid @enderror" required name="email"  placeholder="Email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror    
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Password<sup style="color:red">*</sup></label>
                                    <input type="password" name="password" class="form-control form-control-lg " required placeholder="Password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="mt-3">
                                    <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit" >SIGN IN</button>
                                </div>


                                <div class="text-center mt-4 font-weight-light">
                                    <!--Back to Home? <a href="{{url('/')}}" class="text-primary">Click</a>-->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>

</div>

