@include('layout/header') 
<div class="inr-page detail-page">
    <div class="container">
        <h2>Product Details</h2>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="detail-lft">
                    <div class="detail-img">
                        <img src="{{url('uploaded/products')}}/{{$data['product']->cover}}">
                    </div>
                    <ul>                      
                        @if(!empty($data['productImages']))                      
                        @foreach($data['productImages'] as $img)
                        <li> <img src="{{url('uploaded/products')}}/{{$img->image}}" height="100px" width="100px"> </li>
                        @endforeach
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="detail-rgt">
                    <h3>{{$data['product']->title}}</h3>
                    <div class="prd-amt">{{$data['product']->price}}</div>
                    <div class="prd-size">
                        <label>Sizes:</label>
                        <ul class="size-Filter">
                            @if($data['attributes'])
                            @foreach($data['attributes'] as $att)
                            <li>
                            <level> Qty <level>
                                    <span>{{product_color_qty($data['product']->id,$att->size)}}</span><span class="sizeproduct size-box" data-value="{{$att->size}}">{{$att->size}}</span>
                                    </li>
                                    @endforeach
                                    @endif
                                    </ul>
                                    </div>
                                    <div class="prd-color">
                                        <label>Colors:</label>
                                        <ul class="colurs-Filter">
                                            @if($data['attributes'])
                                            @foreach($data['attributes'] as $att)
                                            <li>
                                            <level> Qty <level>
                                                    <span>{{product_color_qty($data['product']->id,$att->color)}}</span>
                                                    <span class="colorproduct color-box {{$att->color}}" data-value="{{$att->color}}"></span>
                                                    </li>
                                                    @endforeach
                                                    @endif
                                                    <li>

                                                    </li>
                                                    </ul>
                                                    </div>
                                                    <div class="prd-quantity">
                                                        <label>Qty:</label>
                                                        <input type="number" class="pqty" value="1" placeholder="1"/>
                                                    </div>
                                                    <div class="detail-btns">
                                                        <form action="{{url('carts/data')}}" method="post" >
                                                            @csrf
                                                            <input type="hidden" value="{{$data['product']->id}}" name="product_id" class="product_id" >
                                                            <input type="hidden" value="{{$data['attributes'][0]->size}}" name="size" class="size" >
                                                            <input type="hidden" value="{{$data['attributes'][0]->color}}" name="color" class="color" >
                                                            <input type="hidden" value="1" name="qty" class="qty" >
                                                            <input type="hidden" value="{{$data['product']->price}}" name="price" class="price">
                                                            <input type="hidden" value="{{$data['product']->price}}" name="totalprice" class="totalprice">
                                                            <button type="submit" class="btn cartbutton">Add to cart</button>
                                                        </form>
                                                        <a href="#" class="btn buy-btn">Buy now</a>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-12">
                                                        <p class="prd-info">{{$data['product']->description}}</p>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    <input type="hidden" value="{{$data['product']->price}}" class="priceProd">
                                                    @include('layout/footer')

                                                    <script>
                                                        $('.sizeproduct').on('click', function () {
                                                            var size = $('.sizeproduct').data('value');
                                                            $('.size').val(size);
                                                        });
                                                        $('.colorproduct').on('click', function () {
                                                            var color = $('.colorproduct').data('value');
                                                            $('.color').val(color);
                                                        });
                                                        $('.pqty').on('change', function () {
                                                            var pqty = $('.pqty').val();
                                                            var price = $('.priceProd').val();
                                                            var tprice = pqty * price;
                                                            tprice = tprice + .00;
                                                            $('.totalprice').val(tprice);
                                                            $('.qty').val(pqty);
                                                        });
                                                    </script>