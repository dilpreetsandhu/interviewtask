@foreach ($data as $value)
<div class="col-sm-6 col-md-4 col-lg-3">
    <div class="prd-block">
        <div class="prd-img">
            <img src="{{url('uploaded/products/')}}/{{$value->cover}}" height="300px" width="200px">
        </div>
        <h3>{{$value->title}}</h3>
        <div class="prd-amt">{{$value->discount}} <span>{{$value->discount}}</span></div>
        <a href="{{url("products/detail/")}}/{{$value->id}}"> <div  class="btn geturldat">View Details</div></a>
    </div>
</div>
@endforeach
  </div>
        <div class="pagination_div">
            <nav aria-label="Page navigation example">
              {!! $data->links() !!}
            </nav>
        </div>