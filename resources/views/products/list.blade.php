@include('layout/header')
<div class="inr-page prd-list">
    <div class="container">
        <h2>Shop</h2>
         <div class="row" id="products_data">
            @include('products.ajax_load')
      
    </div>
</div>
@include('layout/footer')
 <script>
        $(document).ready(function () {

            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                fetch_data(page);
            });

            function fetch_data(page)
            {
                $.ajax({
                    url: "/products/fetch_data?page=" + page,
                    success: function (data)
                    {
                        $('#products_data').html(data);
                    }
                });
            }

        });
    
    </script>