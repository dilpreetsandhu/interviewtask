@include('layout/header') 

<div class="inr-page cart-page">
    <div class="container">
        <h2>Cart</h2>
        <div class="cart-table table-responsive">
            <table class="table table-bordered " id="thisTable">
                <thead>
                    <tr>
                        <td class="text-center">Image</td>
                        <td class="text-center">Product Name</td>

                        <td class="text-center">Quantity</td>
                        <td class="text-center">Price</td>
                        <td class="text-center">Total</td>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($data))
                    <?php $total = 0; ?>
                    @foreach($data as $cart)

                    <tr class="cartrow" data-id="{{$cart->id}}">
                        <td class="text-center"> <a href="{{url("products/detail/")}}/{{$cart->product_id}}"> <img src="{{url('uploaded/products')}}/{{product_cover($cart->product_id)}}"> </a> </td>
                        <td class="text-center"> <a href="#">{{product_name($cart->product_id)}}</a> </td>
                        <td class="text-center " >
                            <div class="input-group btn-block" style="max-width: 170px; margin:0 auto;">
                                <input type="number" class="getcartdetail" name="qty" data-id="{{$cart->id}}" data-value="{{$cart->price}}" value="{{$cart->qty}}" size="1" min="1"  class="form-control">
                                <button type="button" data-toggle="tooltip" title="" class="btn btn-danger cartRemove" data-id="{{$cart->id}}" data-original-title="Remove"><i class="fa fa-times-circle"></i></button>

                            </div></td>
                        <td class="text-center" >{{$cart->price}}</td>
                        <td class="text-center qtyprice">{{$cart->total_price}}</td>
                    </tr>
                    <?php $total = $total + $cart->price; ?>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="cart_total">
            <ul>

                <li> <div class="text-right"><b>Total:</b></div> <span><b class="totalValue">{{$total}}</b></span> </li>	
            </ul>
            <div class="clear"></div>
        </div>
        <div class="cart-btns">
            <a href="#" class="btn back_btn">Back to Shopping</a>
            <a href="#" class="btn check_btn">Checkout</a>
        </div>
    </div>
</div>
@include('layout/footer')
<script>



//    $('.getcartdetail').on('change', function () {
//        var qty = $(this).val();
//        var price = $(this).data('value');
//        var total = qty * price;
//        var grand = 0;
//        $(this).closest('tr').find('td.qtyprice').text(total);
//    });

    $('.cartRemove').on('click', function () {

        var cid = $(this).data('id');

        var url = "{{url('cart/delete')}}";
        var data = {'cid': cid, '_token': '<?php echo csrf_token() ?>'};
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function (result) {
                window.location.href = "{{url('cart/detail')}}";
            }
        });
    })
    $('.getcartdetail').on('change', function () {
        var qty = $(this).val();
        var cid = $(this).data('id');
        var price = $(this).data('value');
        var total = qty * price;
        var url = "{{url('cart/update')}}";
        var data = {'cid': cid, 'qty': qty, 'price': price, 'total': total, '_token': '<?php echo csrf_token() ?>'};
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function (result) {
                window.location.href = "{{url('cart/detail')}}";
            }
        });
    });

</script>
