  
<!-- JavaScript Libraries -->
<script src="{{url('assets/lib/jquery/jquery.min.js')}}"></script>
<script src="{{url('assets/lib/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/lib/mobile-nav/mobile-nav.js')}}"></script>
<script src="{{url('assets/lib/wow/wow.min.js')}}"></script>
<script src="{{url('assets/lib/owl/js/owl.carousel.js')}}"></script>
<script src="{{url('assets/lib/magnific/js/jquery.magnific-popup.min.js')}}"></script>
<!-- Main Javascript File -->
<script src="{{url('assets/js/main.js')}}"></script>
</body>

</html>