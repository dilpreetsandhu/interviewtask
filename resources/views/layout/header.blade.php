<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Interview Test</title>    
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <!-- Bootstrap CSS File -->
        <link href="{{url('assets/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- Main Stylesheet File -->
        <link href="{{url('assets/css/style.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    
<body>
    <!--==========================
  Header
  ============================-->
    <header id="header" class="inr-hdr">
        <div id="bottom-bar">
            <div class="container-fluid">
                <div class="row top-header">
                    <div class="col-lg-12">
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3   col-4">
                        <div class="logo float-left">
                            <!--use an image logo -->
                            <h1 class="text-light">
                                <a href="#" class="scrollto">
                                    <img src="{{url('assets/img/logo.png')}}" >
                               
                                <li class="cart_num_sm"><a href="{{url('cart/detail')}}"><img src="{{url('assets/img/cart.png')}}"></a> <span >{{cart_count()}}</span></li>
                                </a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-6 col-sm-8 d-lg-none d-block ">
                        <div class="mobile_cart">
                            <ul>
                                
                            </ul>
                        </div>
                    </div>
                   
            </div>	 
        </div>	 
    </header>
    <!-- #header -->