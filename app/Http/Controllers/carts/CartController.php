<?php

namespace App\Http\Controllers\carts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Product;
use Session;

class CartController extends Controller {

    /**
     * @ Function Name      : insert_cart
     * @ Function Params    : post data 
     * @ Function Purpose   : add to cart user selected product .
     * @ Function Returns   : success 
     */
    function insert_cart(Request $req) {
//        echo '<pre>';
//        print_r($req->input()); die;
        $cart = new Cart;
        if (Session::get('login')) {
            $id = Session::get('id');
            $checkcart = $cart::where(['user_id' => $id, 'product_id' => $req->input('product_id')])->first();
            if (empty($checkcart)) {
                $cart->product_id = $req->input('product_id');
                $cart->user_id = $id;
                $cart->size = $req->input('size');
                $cart->color = $req->input('color');
                $cart->qty = $req->input('qty');
                $cart->price = (float) $req->input('price');
                $cart->total_price = (float) $req->input('totalprice');
                $cart->created_at = date('Y-m-d');
                $cart->updated_at = date('Y-m-d');
                $cart->save();
            } else {
                $updatecart['size'] = $req->input('size');
                $updatecart['color'] = $req->input('color');
                $updatecart['qty'] = $req->input('qty');
                $updatecart['price'] = (float) $req->input('price');
                $updatecart['total_price'] = (float) $req->input('totalprice');

                $updatecart['updated_at'] = date('Y-m-d');
                $cart::where(['id' => $checkcart->id])->update($updatecart);
            }
            return redirect('products');
        } else {
            return redirect('login');
        }
    }

    /**
     * @ Function Name      : detail
     * @ Function Params    : 
     * @ Function Purpose   : show single Cart detail.
     * @ Function Returns   : 
     */
    function detail() {
        $cart = new Cart;
        if (Session::get('login')) {
            $id = Session::get('id');

            $checkcart = $cart::where(['user_id' => $id])->get();
            return view('carts.detail', ['data' => $checkcart]);
        } else {
            return redirect('login');
        }
    }

    function update(Request $req) {
        $cart = new Cart;
        $id = $req->input('cid');
        $updatecart['qty'] = $req->input('qty');
        $updatecart['price'] = (float) $req->input('price');
        $updatecart['total_price'] = (float) $req->input('total');

        $updatecart['updated_at'] = date('Y-m-d');
        $cart::where(['id' => $id])->update($updatecart);
        echo json_encode(true);
        die;
    }

    function delete(Request $req) {
        $cart = new Cart;
        $id = $req->input('cid');

        $cart::where(['id' => $id])->delete();
        echo json_encode(true);
        die;
    }

}
