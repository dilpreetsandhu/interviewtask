<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Hash;
use DB;
use Session;

class LoginController extends Controller {

    /**
     * @ Function Name      : index
     * @ Function Params    : 
     * @ Function Purpose   : login user.
     * @ Function Returns   : 
     */
    function index() {
        return view('users.login');
    }

    /**
     * @ Function Name      : login
     * @ Function Params    : email password
     * @ Function Purpose   : check login user detail.
     * @ Function Returns   : 
     */
    function login(Request $req) {
        $validatedData = $req->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $user = User::where(['email' => $req->input('email')])->first();
        if (!empty($user) && Hash::check($req->input('password'), $user->password)) {
            Session::put('login', true);
            Session::put('id', $user->id);
            Session::put('name', $user->name);
            Session::put('role', $user->role);
            if ($user->role_id == '1') {
                return redirect('admin/dashboard');
            } else {
                return redirect('products');
            }
        } else {
            return redirect()->back()->with('error', 'We are sorry but your login credentials do not match!');
        }
    }

}
