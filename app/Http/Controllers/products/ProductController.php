<?php

namespace App\Http\Controllers\products;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Product_image;
use App\Models\Attribute;
use DataTables;
use DB;
use Session;
use Helper;

class ProductController extends Controller {

    /**
     * @ Function Name      : index
     * @ Function Params    :
     * @ Function Purpose   : listing.
     * @ Function Returns   : products data
     */
    function index() {
        $data = Product::paginate(15);
        return view('products.list', compact('data'));
    }

    /**
     * @ Function Name      : fetch_data
     * @ Function Params    : page number
     * @ Function Purpose   : get product listing .
     * @ Function Returns   : products data
     */
    function fetch_data(Request $request) {
        if ($request->ajax()) {
            $data = Product::paginate(15);
            return view('products.ajax_load', compact('data'))->render();
        }
    }

    /**
     * @ Function Name      : detail
     * @ Function Params    : product id
     * @ Function Purpose   : show single product detail.
     * @ Function Returns   : 
     */
    function detail($id) {

        $productData = Product::where('id', $id)->first();
        if (!empty($productData)) {
            $data['title'] = 'Product Detail';
            $data['product'] = $productData;
            $data['productImages'] = Product_image::where('product_id', $id)->get();
            $data['attributes'] = Attribute::where(['product_id' => $id])->get();

            $qty = Attribute::select(DB::raw("SUM(qty) as count"))->where('product_id', $id)->get();
            $data['qty'] = $qty[0]->count;

//            echo '<pre>';
//            print_r($data); die;
            return view('products.detail', ['data' => $data]);
        } else {
            echo 'Not Access';
        }
    }

   

}
