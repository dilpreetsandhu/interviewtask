<?php

use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Attribute;
use App\Models\Cart;
use Illuminate\Support\Facades\Session;

if (!function_exists('product_name')) {

    function product_name($id) {
        $product = Product::where(['id' => $id])->first();
        return $product->title;
    }

}
if (!function_exists('product_cover')) {

    function product_cover($id) {
        $product = Product::where(['id' => $id])->first();
        return $product->cover;
    }

}
if (!function_exists('product_color_qty')) {

    function product_color_qty($pId, $color) {
        $product = Attribute::select(DB::raw('SUM(qty)as count'))->where(['product_id' => $pId, 'color' => $color])->get();

        return $product[0]->count;
    }

}
if (!function_exists('product_size_qty')) {

    function product_size_qty($pId, $size) {
        $product = Attribute::select(DB::raw('SUM(qty)as count'))->where(['product_id' => $pId, 'size' => $size])->get();

        return $product[0]->count;
    }

}
if (!function_exists('cart_count')) {

    function cart_count() {
        if (Session::get('id')) {
            $id = Session::get('id');

            $cart = Cart::where(['user_id' => $id])->get();
            return $cart->count();
        } else {
            return 0;
        }
    }

}